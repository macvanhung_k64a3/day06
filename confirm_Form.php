<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="confirm.css">
</head>

<body>
    <div class="main">
        <div class="search-form">
            <form action="" method="get">
                <div class="form-group">
                    <label class="form-label">Phân khoa</label>
                    <div class="form-input">
                        <select name="falcuty" id="falcuties">
                            <?php
                            $falcuty = array("None" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
                            foreach ($falcuty as $key => $value) {
                                echo '<option value="' . $key . '">' . $value . '</option>';
                            }
                            ?>
                        </select>
                    </div>


                </div>
                <div class="form-group">
                    <label class="form-label">Từ khóa</label>
                    <div class="form-input">
                        <input type="text" name="keyword" id="keyword" value="">
                    </div>

                </div>
                <input type="submit" class="btn" value="Tìm kiếm"></input>
            </form>
        </div>
        <div class="count">
            <div class="student-found">
                <p>Số sinh viên tìm thấy: XXX</p>
            </div>
            <div class="btn">
                <a href="form.php" class="addBtn">Thêm</a>
            </div>
        </div>
        <div class="student-list">
            <table class="student-table">
                <tr class="header">
                    <td style="width:20%">No.</td>
                    <td style="width:50%">Tên sinh viên</td>
                    <td style="width:30%">Khoa</td>
                    <td style="text-align:center;width:10%">Action</td>
                </tr>
                <tr class="student-item">
                    <td>1</td>
                    <td style="">Nguyen Văn Aa</td>
                    <td>Khoa học máy tính</td>
                    <td class="action">
                        <buton class="actionBtn" style="margin-right:8px;">Xóa</buton>
                        <buton class="actionBtn">Sửa</buton>
                    </td>
                </tr>
                <tr class="student-item">
                    <td>2</td>
                    <td style="">Mạc Văn Ba</td>
                    <td>Khoa học máy tính</td>
                    <td class="action">
                        <buton class="actionBtn" style="margin-right:8px;">Xóa</buton>
                        <buton class="actionBtn">Sửa</buton>
                    </td>
                </tr>
                <tr class="student-item">
                    <td>3</td>
                    <td style="">Mạc Văn Ca</td>
                    <td>Khoa học dữ liệu</td>
                    <td class="action">
                        <buton class="actionBtn" style="margin-right:8px;">Xóa</buton>
                        <buton class="actionBtn">Sửa</buton>
                    </td>
                </tr>
                <tr class="student-item">
                    <td>4</td>
                    <td style="">Mạc Văn Da</td>
                    <td>Khoa học dữ liệu</td>
                    <td class="action">
                        <buton class="actionBtn" style="margin-right:8px;">Xóa</buton>
                        <buton class="actionBtn">Sửa</buton>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>

</html>